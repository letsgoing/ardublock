# ArduBlock 2 letsgoING #

## Neuerungen in der Version 2.3 ##

### Basic und Full und Sim Modus ###
Seit Version 2.2 gibt es drei verschiedene Block-Menüs.
#### Basic-Blöcke ####
Das Standard-Menü enthält die für den Einstieg wichtigsten Blöcke.
Die Blöcke sind auf den Grundlagenkurs von [letsgoING](http://letsgoing.org) zugeschnitten.
Über das Kontext-Menue kann die Blockreferenz für den jeweiligen Basic-Blocke geöffnet werden. 
#### Full-Blöcke ####
Das Full-Blockset beinhalten alle Standard-Blöcke und viele zusätzliche Funktionen.
Es können z.B. Unterprogramme erstellt, Interrupts genutzt und Servos oder Schrittmotoren angesteuert werden.
Die Variablen werden um neue Datentypen ergänzt, es gibt mehr Möglichkeiten für die serielle Kommunikation und es kann geschriebener Code eingefügt werden.
#### Sim-Blöcke ####
Das Standard-Menü enthält die für den Einstieg wichtigsten Blöcke.
Die Blöcke sind auf den Grundlagenkurs von [letsgoING](http://letsgoing.org) zugeschnitten.

### Variablen-System ###
Es gibt u. A.
- digitale Variablen (bool) *Standard*
- analoge Variablen (int16) *Standard*
- kurze analoge Variablen (int8)
- lange analoge Variablen (long)
- dezimale analoge Variablen (float)
- Zeichen-Variablen (char) 
- Tabellen für (kurze/lange/dezimale) analoge Variablen (intX/long/float-Array)
- Zeichenketten (char-Array)

Alle Variablen können als
1. globale Variable
2. lokale Variable
3. Konstante
verwendet werden.
Außerdem können die Datentypen in jeweils andere Typen gewandelt werden (cast → im Experten-Menü).

### Workspace zoomen ###
In unserer Version von ArduBlock ist es möglich die Programmierfläche zu vergrößern oder verkleinern.
So kann das Fenster immer der Programmgröße und Bildschirm-Auflösung angepasst werden.

### Papierkorb für nicht mehr benötigte Blöcke ###
In unserer Version von ArduBlock ist es möglich nicht mehr benötigte Blöcke über den Papierkorb zu löschen.
Dies kann aber nicht rückgängig gemacht werden.

### Bild-Export ###
Wird ein Programm als Bild gespeichert, wird das Bild auf die Programmgröße zugeschnitten und mit transparentem Hintergrund gespeichert.

### Copy-Paste ###
Über die Kopieren- und Einfügen-Schaltflächen lassen sich Programme zwischen verschiedenen ArduBlock-Fenstern übertragen.

### ShortKeys ###
Die wichtigsten Funktionen in ArduBlock sind nun über Shortkeys erreichbar (Strg+TASTE).
- Programm öffnen → Strg+O
- Programm speichern → Strg+S
- neues Programm → Strg+N
- Programm hochladen → Strg+U
- Seriellen Monitor öffnen → Strg+M
- Bild speichern → Strg+P
- Blöcke klonen → Strg+Rechtsklick

## Installation in der Arduino IDE ##
1. [Arduino IDE](https://www.arduino.cc/en/Main/Software)  für das eigene Betriebssystem herunterladen (Installations- oder Zip-Version) **ACHTUNG: ArduBlock funktioniert nur bis Version 1.X**
2. ArduBlock2 als ZIP herunterladen und ArduBlockTool-Ordner entpacken
3. a) INSTALLATION: Im Home-Verzeichnis (Sketchbook) der Arduino IDE "tools"-Ordner erstellen und ArduBlockTool-Ordner in das Verzeichnis kopieren
3. b) ZIP-Version: ArduBlockTool-Ordner in das "tools"-Verzeichnis der Arduino IDE kopieren (der "tools"-Ordner liegt im Hauptverzeichnis der Arduino IDE)
4. *Wenn die Q-Touch-Funktionen verwendet werden sollen, wird die [LGI-QTouch Library](https://github.com/letsgoING/Libraries) benötigt.*
5. *Wenn die NeoPixel-Blöcke verwendet werden sollen, wird auch noch die [NeoPixel-Library](https://github.com/adafruit/Adafruit\_NeoPixel/archive/master.zip) von Adafruit benötigt.*
